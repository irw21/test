import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        System.out.println("===Test code BE===");

        //question 1
        int[] nums1 = {3,4,1,2};
        System.out.println(subtracted(nums1));

        //question 2
        int[] nums2 = {1,2,3,4};
        int x = 4;
        System.out.println(divided(nums2, x));

        //question 3
        String word = "souvenir loud four lost";
        x = 4;
        System.out.println(strContain(word, x));
    }

    private static List<Integer> subtracted(int[] nums) {
        List<Integer> subValues = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(i != j){
                    if(nums[i] - nums[j] < 0) break;
                }
                if(nums.length - 1 == j){
                    subValues.add(nums[i]);
                }
            }
        }
        return subValues;
    }

    private static List<Integer> divided(int[] nums, int x) {
        List<Integer> divValues = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(i != j){
                    if((float)nums[i] / nums[j] == x) break;
                }
                if(nums.length - 1 == j){
                    divValues.add(nums[i]);
                }
            }
        }
        return divValues;
    }

    private static List<String> strContain(String word, int x) {
        List<String> strValues = new ArrayList<>();
        String[] strSplit = word.split("[ ]");
        for (String data : strSplit) {
            if (data.length() == x) strValues.add(data);
        }
        return strValues;
    }
}
